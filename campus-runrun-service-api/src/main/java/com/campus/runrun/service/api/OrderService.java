package com.campus.runrun.service.api;

import com.baomidou.mybatisplus.extension.service.IService;
import com.campus.runrun.common.utils.PageResult;
import com.campus.runrun.model.entity.ErrandOrderEntity;
import com.campus.runrun.model.vo.OrderShowVO;

import java.util.List;

public interface OrderService extends IService<ErrandOrderEntity> {

    void save(int errandId);

    void confirm(int orderId);

    PageResult getAll(int page, int size);

    PageResult getByFlag(int flag, int page, int size);

    void cancelByEr(int orderId);

    void finish(int orderId);

    List<OrderShowVO> getWeOrder(int employeeId);

    void cancelByEe(int orderId);
}

