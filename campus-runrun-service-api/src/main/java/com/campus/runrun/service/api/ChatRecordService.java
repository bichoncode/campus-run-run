package com.campus.runrun.service.api;

import com.baomidou.mybatisplus.extension.service.IService;
import com.campus.runrun.common.utils.PageResult;
import com.campus.runrun.model.entity.ChatRecordEntity;
import com.campus.runrun.model.requestvo.ChatRecordQueryParam;
import com.campus.runrun.model.vo.ChatRecordSendVO;

/**
 * 
 *
 * @author BichonCode
 * @email chenzhichaohh@163.com
 * @date 2020-10-31 17:12:39
 */
public interface ChatRecordService extends IService<ChatRecordEntity> {

    /**
     * 通过对方id，分页查询聊天记录
     * @return
     */
    PageResult obtainRecordsPageByOppositeUserId(ChatRecordQueryParam chatRecordQueryParam);

    /**
     * 插入一条聊天记录
     * @param chatRecordSendVO
     * @return
     */
    ChatRecordEntity addOne(ChatRecordSendVO chatRecordSendVO);

    /**
     * 将一条聊天记录设置为已读
     */
    void setHasReadByRecordId(Integer id);
}

