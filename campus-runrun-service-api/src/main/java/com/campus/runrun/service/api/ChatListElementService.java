package com.campus.runrun.service.api;

import com.baomidou.mybatisplus.extension.service.IService;
import com.campus.runrun.common.utils.PageResult;
import com.campus.runrun.model.entity.ChatListElementEntity;

/**
 * 
 *
 * @author BichonCode
 * @email chenzhichaohh@163.com
 * @date 2020-10-31 17:12:39
 */
public interface ChatListElementService extends IService<ChatListElementEntity> {
    PageResult getChatPageList(int page, int size);
}

