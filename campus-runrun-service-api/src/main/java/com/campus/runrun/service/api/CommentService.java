package com.campus.runrun.service.api;

import com.baomidou.mybatisplus.extension.service.IService;
import com.campus.runrun.common.utils.PageResult;
import com.campus.runrun.model.entity.CommentEntity;
import com.campus.runrun.model.vo.CommentVO;

import java.util.List;


/**
 * 
 *
 * @author BichonCode
 * @email chenzhichaohh@163.com
 * @date 2020-10-31 17:12:39
 */
public interface CommentService extends IService<CommentEntity> {

    List<CommentEntity> getByErrandOrderId(Integer orderId);

    void save(Integer orderId, String content);

    PageResult getCommentsByEmployeeId(Integer employeeId, Integer page, Integer size);
}

