package com.campus.runrun.service.api;

import com.baomidou.mybatisplus.extension.service.IService;
import com.campus.runrun.common.utils.PageResult;
import com.campus.runrun.model.entity.ErrandEntity;
import com.campus.runrun.model.vo.ErrandFirstPageShowVO;
import com.campus.runrun.model.vo.ErrandInsertVO;
import com.campus.runrun.model.vo.ErrandUpadteVO;

import java.util.List;

/**
 *
 *
 * @author BichonCode
 * @email chenzhichaohh@163.com
 * @date 2020-10-31 17:12:39
 */
public interface ErrandService extends IService<ErrandEntity> {

    PageResult showByTime(int page, int size, String content);

    void save(ErrandInsertVO errandInsertVO);

    void update(ErrandUpadteVO errandUpadteVO);

    PageResult searchByPrice(int page, int size, int type, String content);

    PageResult getMyAll(int page, int size);

    PageResult getMyUnOrder(int page, int size);

    List<ErrandFirstPageShowVO> getUserUnOrder(int userId);

    void cancel(Integer errandId);
}

