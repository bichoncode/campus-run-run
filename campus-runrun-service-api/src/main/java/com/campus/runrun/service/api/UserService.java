package com.campus.runrun.service.api;

import com.baomidou.mybatisplus.extension.service.IService;
import com.campus.runrun.model.entity.UserEntity;
import com.campus.runrun.model.vo.UserShowVO;
import com.campus.runrun.model.vo.UserUpdateVO;


/**
 *
 *
 * @author BichonCode
 * @email chenzhichaohh@163.com
 * @date 2020-10-31 17:12:39
 */
public interface UserService extends IService<UserEntity> {
    UserEntity getUserByOpenId(String openId);
    UserShowVO showUser(int id);
    void updateUser(UserUpdateVO userUpdateVO);
}

