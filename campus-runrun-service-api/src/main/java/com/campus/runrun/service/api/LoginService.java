package com.campus.runrun.service.api;


import com.campus.runrun.model.vo.UserLoginReqVO;
import com.campus.runrun.model.vo.UserVO;


/**
 * 登陆模块
 *
 * @author BichonCode
 * @email chenzhichaohh@163.com
 * @date 2020-07-31 00:55:44
 */
public interface LoginService {

    UserVO wxMiniProGramLogin(UserLoginReqVO userLoginReqVO);
}

