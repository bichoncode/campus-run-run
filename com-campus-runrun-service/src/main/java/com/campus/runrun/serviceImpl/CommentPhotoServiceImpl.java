package com.campus.runrun.serviceImpl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.campus.runrun.dao.CommentPhotoDao;
import com.campus.runrun.model.entity.CommentPhotoEntity;
import com.campus.runrun.service.api.CommentPhotoService;
import org.springframework.stereotype.Service;

@Service("commentPhotoService")
public class CommentPhotoServiceImpl extends ServiceImpl<CommentPhotoDao, CommentPhotoEntity> implements CommentPhotoService {
}