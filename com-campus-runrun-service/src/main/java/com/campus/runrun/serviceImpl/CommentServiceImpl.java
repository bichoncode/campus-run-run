package com.campus.runrun.serviceImpl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.campus.runrun.common.exception.CustomException;
import com.campus.runrun.common.exception.ResultCodeEnum;
import com.campus.runrun.common.shiro.JWTFilter;
import com.campus.runrun.common.utils.PageResult;
import com.campus.runrun.dao.CommentDao;
import com.campus.runrun.dao.OrderDao;
import com.campus.runrun.model.entity.CommentEntity;
import com.campus.runrun.model.entity.ErrandOrderEntity;
import com.campus.runrun.model.vo.CommentVO;
import com.campus.runrun.service.api.CommentService;
import com.campus.runrun.service.api.OrderService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.apache.shiro.authz.UnauthorizedException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.xml.stream.events.Comment;
import java.util.List;

@Service("commentService")
public class CommentServiceImpl extends ServiceImpl<CommentDao, CommentEntity> implements CommentService {

    @Autowired
    private CommentDao commentDao;

    @Autowired
    private OrderDao orderDao;

    @Override
    public List<CommentEntity> getByErrandOrderId(Integer orderId) { ;
        QueryWrapper<CommentEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("order_id", orderId);
        List<CommentEntity> commentList = commentDao.selectList(queryWrapper);
        return commentList;
    }

    @Override
    public void save(Integer orderId, String content) {
        QueryWrapper<ErrandOrderEntity> orderEntityQueryWrapper = new QueryWrapper<>();
        orderEntityQueryWrapper.eq("id", orderId.intValue());
        ErrandOrderEntity errandOrderEntity = orderDao.selectOne(orderEntityQueryWrapper);
        if (errandOrderEntity == null) {
            throw new CustomException(ResultCodeEnum.ERRAND_ORDER_NOT_EXIT);
        }
        int userId = JWTFilter.getUserInfo().getId().intValue();
        boolean flag = errandOrderEntity.getEmployerId().intValue() == userId? true : false;
        if (!flag) {
            throw new UnauthorizedException();
        }
        CommentEntity commentEntity = new CommentEntity();
        commentEntity.setOrderId(orderId);
        commentEntity.setContent(content);
        commentEntity.setUserId(userId);
        this.baseMapper.insert(commentEntity);
    }

    @Override
    public PageResult getCommentsByEmployeeId(Integer employeeId, Integer page, Integer size) {
        if (page < 0) {
            page = 1;
        }
        if (size <= 0) {
            size = 4;
        }
        PageHelper.startPage(page, size);
        Page<CommentVO> commentVOList = commentDao.getCommentsByEmployeeId(employeeId);
        PageResult pageResult = new PageResult(commentVOList.getResult(),
                (int) commentVOList.getTotal(), size,
                page);
        return pageResult;
    }
}