package com.campus.runrun.serviceImpl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.campus.runrun.common.exception.CustomException;
import com.campus.runrun.common.exception.ResultCodeEnum;
import com.campus.runrun.dao.UserDao;
import com.campus.runrun.model.entity.UserEntity;
import com.campus.runrun.model.vo.UserShowVO;
import com.campus.runrun.model.vo.UserUpdateVO;
import com.campus.runrun.service.api.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

@Service("userService")
public class UserServiceImpl extends ServiceImpl<UserDao, UserEntity> implements UserService {

    @Override
    public UserEntity getUserByOpenId(String openId) {
        QueryWrapper<UserEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("open_id", openId);
        UserEntity userEntity = baseMapper.selectOne(queryWrapper);
        return userEntity;
    }

    @Override
    public UserShowVO showUser(int id) {
        UserEntity userEntity=baseMapper.selectById(id);
        UserShowVO userShowVO=new UserShowVO();
        BeanUtils.copyProperties(userEntity, userShowVO);
        return userShowVO;
    }

    @Override
    public void updateUser(UserUpdateVO userUpdateVO) {
        UserEntity entity=this.baseMapper.selectById(userUpdateVO.getId());
        if (entity == null) {
            throw new CustomException(ResultCodeEnum.ERRAND_NOT_EXIT);
        }
        UserEntity userEntity=new UserEntity();
        BeanUtils.copyProperties(userUpdateVO, userEntity);
        baseMapper.updateById(userEntity);
    }

}
