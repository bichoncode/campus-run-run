package com.campus.runrun.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.campus.runrun.model.entity.ChatRecordEntity;
import com.campus.runrun.model.vo.ChatRecordToClientVO;
import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 
 * 
 * @author BichonCode
 * @email chenzhichaohh@163.com
 * @date 2020-10-31 17:12:39
 */
@Mapper
@Repository
public interface ChatRecordDao extends BaseMapper<ChatRecordEntity> {

    // 分页查询点对点的聊天记录
    Page<ChatRecordToClientVO> getRecordsPageByOppositeUserId(Integer fromUserId, Integer toUserId);

    // 通过自己的id和对方的id查询最后一条消息记录
    ChatRecordEntity getLastRecordByFromUserIdAndToUserId(Integer fromUserId, Integer toUserId);

    //
    void setHasReadByFromUserIdAndToUserIdBatch(Integer fromUserId, Integer toUserId);

}
