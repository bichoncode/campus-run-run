package com.campus.runrun.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.campus.runrun.model.entity.ErrandEntity;
import com.campus.runrun.model.vo.ChatRecordToClientVO;
import com.campus.runrun.model.vo.ErrandFirstPageShowVO;
import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface ErrandDao extends BaseMapper<ErrandEntity> {
    Page<ErrandFirstPageShowVO> showByTime(String content);


    Page<ErrandFirstPageShowVO> searchByPriceDesc(String content);
    Page<ErrandFirstPageShowVO> searchByPriceAsc(String content);
}
