package com.campus.runrun.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.campus.runrun.model.entity.CommentPhotoEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author BichonCode
 * @email chenzhichaohh@163.com
 * @date 2020-10-31 17:12:39
 */
@Mapper
public interface CommentPhotoDao extends BaseMapper<CommentPhotoEntity> {
	
}
