package com.campus.runrun.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.campus.runrun.model.entity.CommentEntity;
import com.campus.runrun.model.vo.CommentVO;
import com.campus.runrun.model.vo.ErrandFirstPageShowVO;
import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 
 * 
 * @author BichonCode
 * @email chenzhichaohh@163.com
 * @date 2020-10-31 17:12:39
 */
@Mapper
@Repository
public interface CommentDao extends BaseMapper<CommentEntity> {
    Page<CommentVO> getCommentsByEmployeeId(Integer employeeId);
}
