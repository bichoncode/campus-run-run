package com.campus.runrun.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.campus.runrun.model.entity.ChatListElementEntity;
import com.campus.runrun.model.vo.ChatListElementVO;
import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 
 * 
 * @author BichonCode
 * @email chenzhichaohh@163.com
 * @date 2020-10-31 17:12:39
 */
@Mapper
@Repository
public interface ChatListElementDao extends BaseMapper<ChatListElementEntity> {
    // 分页查询点对点的聊天列表
    Page<ChatListElementVO> getChatPageList(Integer fromUserId);
}
