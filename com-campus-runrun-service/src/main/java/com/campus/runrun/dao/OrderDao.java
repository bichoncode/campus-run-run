package com.campus.runrun.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.campus.runrun.model.entity.ErrandOrderEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 
 * 
 * @author BichonCode
 * @email chenzhichaohh@163.com
 * @date 2020-10-31 17:12:39
 */
@Mapper
@Repository
public interface OrderDao extends BaseMapper<ErrandOrderEntity> {
}
