package com.campus.runrun.nettychat;


import com.campus.runrun.model.vo.ChatRecordSendVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel
public class NettyMessage {

    @ApiModelProperty("消息类型")
    private Integer type;

    @ApiModelProperty("消息内容")
    private ChatRecordSendVO chatRecordSendVO;

    @ApiModelProperty("扩展消息，暂时无用")
    private Object ext;
}
