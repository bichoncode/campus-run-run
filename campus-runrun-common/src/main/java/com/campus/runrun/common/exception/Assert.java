package com.campus.runrun.common.exception;

/**
 * 自定义断言类
 * @author BichonCode
 * @mail chenzhichaohh@163.com
 * @create 2020/07/31
 */
public class Assert {


    /**
     * 判断object是否为空，并返回响应的信息
     * 注意，要先定义好对应的错误枚举类型
     * @param object
     */
    public static void assertIsNull(Object object, ResultCodeEnum resultCodeEnum) {
        if (object == null) {
            throw new CustomException(resultCodeEnum);
    }
    };


    /**
     * 是否为真，假则返回错误类型
     * 注意，要先定义好对应的错误枚举类型
     * @param flag
     */
    public static void isTrue(boolean flag, ResultCodeEnum resultCodeEnum) {
        if (!flag) {
            throw new CustomException(resultCodeEnum);
        }
    };


    /**
     * 是否大于零, 例如数据库更新/增加操作，
     * 如果返回影响的行数，则可以运用此方法判断是否操作成功
     * 注意，要先定义好对应的错误枚举类型
     * @param number
     */
    public static void isGreatThanZero(int number, ResultCodeEnum resultCodeEnum) {
        if (number >0) {
            throw new CustomException(resultCodeEnum);
        }
    };

    /**
     * 判断增删改查操作的用户是否合法
      * @param legalUserId
     * @param requestUserId
     */
    public static void assertUser(int legalUserId, int requestUserId) {
        if (legalUserId != requestUserId) {
            throw new CustomException(ResultCodeEnum.ILLEGAL_USER_ERROR);
        }
    };

}
