package com.campus.runrun.common.exception;

import lombok.Getter;
import lombok.ToString;

/**
 * @author BichonCode
 * @email chenzhichaohh@163.com
 * @date 2020-07-31 00:55:44
 */
@Getter
@ToString
public enum ResultCodeEnum {

    SUCCESS(true, 1,"请求成功"),
    UNKNOWN_REASON(false, 20001, "未知错误"),


    BAD_SQL_GRAMMAR(false, 21001, "sql语法错误"),
    JSON_PARSE_ERROR(false, 21002, "json解析异常"),
    PARAM_ERROR(false, 21003, "参数不正确"),
    NULL_ERROR(false, 21004, "空值异常"),


    FILE_UPLOAD_ERROR(false, 210010, "文件上传错误"),
    FILE_DELETE_ERROR(false, 210011, "文件刪除错误"),
    EXCEL_DATA_IMPORT_ERROR(false, 21012, "Excel数据导入错误"),

    VIDEO_UPLOAD_ALIYUN_ERROR(false, 22001, "视频上传至阿里云失败"),
    VIDEO_UPLOAD_TOMCAT_ERROR(false, 22002, "视频上传至业务服务器失败"),
    VIDEO_DELETE_ALIYUN_ERROR(false, 22003, "阿里云视频文件删除失败"),
    FETCH_VIDEO_UPLOADAUTH_ERROR(false, 22004, "获取上传地址和凭证失败"),
    REFRESH_VIDEO_UPLOADAUTH_ERROR(false, 22005, "刷新上传地址和凭证失败"),
    FETCH_PLAYAUTH_ERROR(false, 22006, "获取播放凭证失败"),

    URL_ENCODE_ERROR(false, 23001, "URL编码失败"),
    ILLEGAL_CALLBACK_REQUEST_ERROR(false, 23002, "非法回调请求"),
    FETCH_ACCESSTOKEN_FAILD(false, 23003, "获取accessToken失败"),
    FETCH_USERINFO_ERROR(false, 23004, "获取用户信息失败"),
    LOGIN_ERROR(false, 23005, "登录失败"),
    ILLEGAL_USER_ERROR(false, 23006, "非法用户操作"),

    COMMENT_EMPTY(false, 24006, "评论内容必须填写"),

    PAY_RUN(false, 25000, "支付中"),
    PAY_UNIFIEDORDER_ERROR(false, 25001, "统一下单错误"),
    PAY_ORDERQUERY_ERROR(false, 25002, "查询支付结果错误"),

    ORDER_EXIST_ERROR(false, 25003, "课程已购买"),

    GATEWAY_ERROR(false, 26000, "服务不能访问"),

    CODE_ERROR(false, 28000, "验证码错误"),

    LOGIN_PHONE_ERROR(false, 28009, "手机号码不正确"),
    LOGIN_MOBILE_ERROR(false, 28001, "账号不正确"),
    LOGIN_PASSWORD_ERROR(false, 28008, "密码不正确"),
    LOGIN_DISABLED_ERROR(false, 28002, "该用户已被禁用"),
    REGISTER_MOBLE_ERROR(false, 28003, "手机号已被注册"),
    LOGIN_AUTH(false, 28004, "需要登录"),
    LOGIN_ACL(false, 28005, "没有权限"),
    SMS_SEND_ERROR(false, 28006, "短信发送失败"),
    SMS_SEND_ERROR_BUSINESS_LIMIT_CONTROL(false, 28007, "短信发送过于频繁"),
    MY_VALUE(false, 30000, "我的错误"),

    CODE2SESSION_ERROR(false, 40001, "code2session失败"),
    MISSING_TOKEN_ERROR(false, 40002, "缺少token"),
    UNAUTHORIZED_ERROR(false, 40003, "无权访问"),
    ILLEGAL_TOKEN_ERROR(false, 40004, "非法token"),

    ERRAND_ID_NOT_FIND(false,50001,"errandId不存在！"),
    ERRAND_HAS_RECEIVED(false,50002,"该任务已被接单。"),

    ORDER_ID_NOT_FIND(false,60001,"orderId不存在！"),
    ORDER_EM_NOT_MATCH(false,60002,"订单与雇佣者不匹配。"),
    ORDER_STATUS_NOT_FIT(false,60003,"订单状态不适合该操作。"),
    SEND_TO_SELF(false,60004,"请不要给自己发信息"),
    ERRAND_NOT_EXIT(false,60005,"该跑腿需求不存在。"),
    ERRAND_ORDER_NOT_EXIT(false,60006,"该跑腿订单不存在。"),
    ERRAND_ORDER_HAS_SUCCESS(false,60007,"订单已经交易成功，不能被取消。"),
    ORDER_EE_NOT_MATCH(false,60008,"订单与跑腿者不匹配。"),
    USER_NOT_FIND(false,70001,"找不到用户。");

    private Boolean success;

    private Integer code;

    private String message;

    ResultCodeEnum(Boolean success, Integer code, String message) {
        this.success = success;
        this.code = code;
        this.message = message;
    }
}
