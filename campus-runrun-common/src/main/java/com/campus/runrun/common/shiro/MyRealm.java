package com.campus.runrun.common.shiro;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import java.util.concurrent.TimeUnit;

/**
 * 自定义的Realm用来实现用户的认证和授权
 * 父类AuthenticatingRealm 只做用户认证（登录）
 */
@Slf4j
public class MyRealm extends AuthorizingRealm {
    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * @param token
     * @return
     */
    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof JwtToken;
    }


    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        String token = (String) authenticationToken.getCredentials();
        // 根据token解密，解密出jwt-id，先从redis中查出redisToken，判断是否相同
        String redisToken = (String) redisTemplate.opsForValue().get(JWTUtil.getJwtIdByToken(token));
        /*if (!token.equals(redisToken)) {
            throw new RuntimeException("token无效或者已过期");
        }*/
        // 延长token有效时间
        JWTUtil.acceptExpiresAt(token);
        return new SimpleAuthenticationInfo(token, token, "myRealm");
    }


    /**
     * 用户授权的方法， 当用户认证通过每次访问需要授权的请求时都需要执行这段代码来完后曾授权操作
     * 这里用该查询数据库来获取当前用户的所有角色和权限，并设置到shiro中
     *
     * @param principalCollection
     * @return
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        // 定义用户角色的set集合这个集合应该来自数据库
        // 注意：由于每次点击需要授权的请求时，Shiro都会执行这个方法，因此如果这里的数据时来自于数据库中的
        //     那么一定要控制好不能每次都从数据库中获取数据这样效率太低了
        // 设置权限，这里个操作应该是用数据中读取数据
        String token = (String) principalCollection.getPrimaryPrincipal();
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        return info;
    }


}
