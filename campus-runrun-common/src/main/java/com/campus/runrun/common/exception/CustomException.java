package com.campus.runrun.common.exception;


import lombok.Getter;

/**
 * @author BichonCode
 * @email chenzhichaohh@163.com
 * @date 2020-07-31 00:55:44
 */
@Getter
public class CustomException extends RuntimeException {
    // 错误代码
    private ResultCodeEnum resultCodeEnum;


    public CustomException(ResultCodeEnum resultCodeEnum) {
        super(resultCodeEnum.getMessage());
        this.resultCodeEnum = resultCodeEnum;
    }

    public ResultCodeEnum getResultCode() {
        return resultCodeEnum;
    }
}
