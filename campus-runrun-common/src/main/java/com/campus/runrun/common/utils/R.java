/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 * <p>
 * https://www.renren.io
 * <p>
 * 版权所有，侵权必究！
 */

package com.campus.runrun.common.utils;

import com.campus.runrun.common.exception.ResultCodeEnum;
import org.apache.http.HttpStatus;

import java.util.HashMap;

/**
 * 返回数据
 *
 * @author Mark sunlightcs@gmail.com
 */
public class R extends HashMap<String, Object> {
    private static final long serialVersionUID = 1L;

    public R() {
    }

    // 默认异常处理
    public static R error() {
        return error(false, HttpStatus.SC_INTERNAL_SERVER_ERROR, "未知异常，请联系管理员");
    }

    // 具体异常处理
    public static R error(ResultCodeEnum resultCodeEnum) {
        R r = new R();
        r.put("success", resultCodeEnum.getSuccess());
        r.put("code", resultCodeEnum.getCode());
        r.put("message", resultCodeEnum.getMessage());
        return r;
    }

    public static R error(String msg) {
        return error(false, HttpStatus.SC_INTERNAL_SERVER_ERROR, msg);
    }

    public static R error(boolean success, int code, String message) {
        R r = new R();
        r.put("success", false);
        r.put("code", code);
        r.put("message", message);
        return r;
    }




    public static R ok() {
        R r = new R();
        r.put("success", true);
        r.put("code", 1);
        r.put("message", "操作成功");
        return r;
    }

    public R put(String key, Object value) {
        super.put(key, value);
        return this;
    }

    public Integer getCode() {

        return (Integer) this.get("code");
    }

}
