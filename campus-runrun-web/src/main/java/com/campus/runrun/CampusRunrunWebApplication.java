package com.campus.runrun;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.campus.runrun.dao")
public class CampusRunrunWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(CampusRunrunWebApplication.class, args);
	}

}
