package com.campus.runrun.controller;


import com.campus.runrun.common.utils.PageResult;
import com.campus.runrun.common.utils.R;
import com.campus.runrun.model.entity.ChatListElementEntity;
import com.campus.runrun.model.vo.ChatListElementVO;
import com.campus.runrun.service.api.ChatListElementService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

/**
 * 
 *
 * @author BichonCode
 * @email chenzhichaohh@163.com
 * @date 2020-10-31 17:12:39
 */
@RequestMapping("/api/chatListElement")
@RestController
@Api(tags = "聊天列表模块")
public class ChatListElementController {
    @Autowired
    private ChatListElementService chatListElementService;

    @GetMapping("/getChatList")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "校验token", name = "Authorization", paramType = "header", required = true)
    })
    @ApiOperation(value = "查询聊列表", notes = "")
    public R getChatList(int page, int size) {
        PageResult chatPageList = chatListElementService.getChatPageList(page, size);
        return R.ok().put("data", chatPageList);
    }

}
