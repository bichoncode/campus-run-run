package com.campus.runrun.controller;


import com.campus.runrun.common.utils.PageResult;
import com.campus.runrun.common.utils.R;
import com.campus.runrun.model.vo.OrderShowVO;
import com.campus.runrun.service.api.OrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags = "订单模块")
@RestController
@RequestMapping("api/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @PostMapping("/save")
    @ApiOperation(value = "跑腿人点击【确认接单】")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "校验token", name = "Authorization", paramType = "header", required = true)
    })
    public R save(Integer errandId) {
        orderService.save(errandId);
        return R.ok();
    }

    @PutMapping("/confirm/{orderId}")
    @ApiOperation(value = "跑腿服务人点击【确定订单】")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "校验token", name = "Authorization", paramType = "header", required = true)
    })
    public R confirm(@PathVariable Integer orderId) {
        orderService.confirm(orderId);
        return R.ok();
    }

    @DeleteMapping("/cancelByEr/{orderId}")
    @ApiOperation(value = "雇佣方取消订单", notes = "在被某跑腿接单后，自己尚未同意之前调用")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "校验token", name = "Authorization", paramType = "header", required = true)
    })
    public R cancelByEr(@PathVariable Integer orderId) {
        orderService.cancelByEr(orderId);
        return R.ok();
    }

    @PutMapping("/finish/{orderId}")
    @ApiOperation(value = "雇佣方确认完成订单", notes = "订单需要在【交易中】的状态（status=20）")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "校验token", name = "Authorization", paramType = "header", required = true)
    })
    public R finish(@PathVariable Integer orderId) {
        orderService.finish(orderId);
        return R.ok();
    }

    @GetMapping("/getAll")
    @ApiOperation(value = "查询用户的所有订单")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "校验token", name = "Authorization", paramType = "header", required = true)
    })
    public R getAll(int page, int size) {
        PageResult orderList = orderService.getAll(page, size);
        return R.ok().put("orderList", orderList);
    }

    @GetMapping("/getReceive")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "校验token", name = "Authorization", paramType = "header", required = true)
    })
    @ApiOperation(value = "用户获取【接单】/【发布】列表", notes = "flag为0查询接单列表，flag为1查询发布列表")
    public R getReceive(int flag, int page, int size) {
        PageResult orderShowVOList = orderService.getByFlag(flag, page, size);
        return R.ok().put("orderList", orderShowVOList);
    }

    @GetMapping("/getWeOrder/{employeeId}")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "校验token", name = "Authorization", paramType = "header", required = true)
    })
    @ApiOperation(value = "雇佣方查询双方之间【未确认】的订单列表")
    public R getWeOrder(@PathVariable Integer employeeId) {
        List<OrderShowVO> orderShowVOList = orderService.getWeOrder(employeeId);
        return R.ok().put("orderList", orderShowVOList);
    }

    @PutMapping("/cancelByEe/{orderId}")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "校验token", name = "Authorization", paramType = "header", required = true)
    })
    @ApiOperation(value = "跑腿方在雇佣方尚未【确认订单】之前取消订单")
    public R cancelByEe(@PathVariable Integer orderId) {
        orderService.cancelByEe(orderId);
        return R.ok();
    }
}
