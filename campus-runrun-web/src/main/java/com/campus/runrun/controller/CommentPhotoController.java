package com.campus.runrun.controller;

import java.util.Arrays;
import java.util.Map;

import com.campus.runrun.common.utils.R;
import com.campus.runrun.model.entity.CommentPhotoEntity;
import com.campus.runrun.service.api.CommentPhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;



/**
 * 
 *
 * @author BichonCode
 * @email chenzhichaohh@163.com
 * @date 2020-10-31 17:12:39
 */
@RestController
@RequestMapping("provider/commentphoto")
public class CommentPhotoController {
    @Autowired
    private CommentPhotoService commentPhotoService;



    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("provider:commentphoto:info")
    public R info(@PathVariable("id") Integer id){
		CommentPhotoEntity commentPhoto = commentPhotoService.getById(id);

        return R.ok().put("commentPhoto", commentPhoto);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("provider:commentphoto:save")
    public R save(@RequestBody CommentPhotoEntity commentPhoto){
		commentPhotoService.save(commentPhoto);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
   // @RequiresPermissions("provider:commentphoto:update")
    public R update(@RequestBody CommentPhotoEntity commentPhoto){
		commentPhotoService.updateById(commentPhoto);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("provider:commentphoto:delete")
    public R delete(@RequestBody Integer[] ids){
		commentPhotoService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
