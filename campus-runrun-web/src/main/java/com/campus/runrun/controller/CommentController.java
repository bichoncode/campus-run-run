package com.campus.runrun.controller;

import java.util.Arrays;
import java.util.List;

import com.campus.runrun.common.utils.PageResult;
import com.campus.runrun.common.utils.R;
import com.campus.runrun.model.entity.CommentEntity;
import com.campus.runrun.model.vo.CommentVO;
import com.campus.runrun.service.api.CommentService;
import io.swagger.annotations.*;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * 
 *
 * @author BichonCode
 * @email chenzhichaohh@163.com
 * @date 2020-10-31 17:12:39
 */
@Api(tags = "评论接口")
@RestController
@RequestMapping("api/comment")
public class CommentController {
    @Autowired
    private CommentService commentService;



    /**
     * 信息
     */
    @GetMapping("/getByErrandOrderId/{orderId}")
    @ApiOperation(value = "查询评论")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "校验token", name = "Authorization", paramType = "header", required = true)
    })
    public R getByErrandOrderId(@PathVariable("orderId") Integer orderId){
        List<CommentEntity> commentList = commentService.getByErrandOrderId(orderId);
        return R.ok().put("data", commentList);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    @ApiOperation(value = "写评论", notes = "orderId为订单id, content为评论内容")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "校验token", name = "Authorization", paramType = "header", required = true)
    })
    public R save(@ApiParam("订单号")Integer orderId, @ApiParam("评论内容")String content ){
		commentService.save(orderId, content);
        return R.ok();
    }


    /**
     * 保存
     */
    @GetMapping("/getCommentsByEmployeeId")
    @ApiOperation(value = "获取用户的所有被评论", notes = "orderId为订单id, content为评论内容")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "校验token", name = "Authorization", paramType = "header", required = true)
    })
    public R getCommentsByEmployeeId(Integer employeeId, Integer page, Integer size){
        PageResult commentVOList = commentService.getCommentsByEmployeeId(employeeId, page, size);
        return R.ok().put("data", commentVOList);
    }


}
