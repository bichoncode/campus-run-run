package com.campus.runrun.controller;


import com.campus.runrun.common.utils.PageResult;
import com.campus.runrun.common.utils.R;
import com.campus.runrun.model.vo.ErrandFirstPageShowVO;
import com.campus.runrun.model.vo.ErrandInsertVO;
import com.campus.runrun.model.vo.ErrandUpadteVO;
import com.campus.runrun.service.api.ErrandService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *
 *
 * @author BichonCode
 * @email chenzhichaohh@163.com
 * @date 2020-10-31 17:12:39
 */
@Api(tags = "跑腿相关接口")
@RestController
@RequestMapping("api/errand")
public class ErrandController {
    @Autowired
    private ErrandService errandService;



    /**
     * 查看首页的发布的跑腿需求
     */
    @GetMapping("/show")
    @ApiOperation(value = "查看首页的发布的跑腿需求, 根据发布时间降序")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "校验token", name = "Authorization", paramType = "header", required = true)
    })
    public R showByTime(int page, int size, String content){
        PageResult pageResult = errandService.showByTime( page,  size, content);
        return R.ok().put("data", pageResult);
    }

    /**
     * 新增跑腿需求
     */
    @PostMapping("/save")
    @ApiOperation(value = "新增跑腿需求")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "校验token", name = "Authorization", paramType = "header", required = true)
    })
    public R save(ErrandInsertVO errandInsertVO){
        errandService.save(errandInsertVO);
        return R.ok();
    }

    /**
     * 修改跑腿发布
     */
    @PostMapping("/update")
    @ApiOperation(value = "修改跑腿需求")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "校验token", name = "Authorization", paramType = "header", required = true)
    })
    public R update(ErrandUpadteVO errandUpdateVO){
        errandService.update(errandUpdateVO);
        return R.ok();
    }


    @GetMapping("/searchByPrice")
    @ApiOperation(value = "根据价格升序或者降序, type = 1 表示升序，0表示降序, content表示搜索的关键字")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "校验token", name = "Authorization", paramType = "header", required = true)
    })
    public R searchByPrice(int page, int size, int type, String content){
        PageResult pageResult = errandService.searchByPrice(page, size, type, content);
        return R.ok().put("data", pageResult);
    }

    @GetMapping("/getMyAll")
    @ApiOperation(value = "获取用户自己发布的的跑腿需求")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "校验token", name = "Authorization", paramType = "header", required = true)
    })
    public R getMyAll(int page, int size){
        PageResult pageResult = errandService.getMyAll(page, size);
        return R.ok().put("data", pageResult);
    }

    @GetMapping("/getMyUnOrder")
    @ApiOperation(value = "获取用户自己发布的【未被接单】的跑腿需求")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "校验token", name = "Authorization", paramType = "header", required = true)
    })
    public R getMyUnOrder(int page, int size){
        PageResult pageResult = errandService.getMyUnOrder(page, size);
        return R.ok().put("data", pageResult);
    }

    @GetMapping("/getUserUnOrder")
    @ApiOperation(value = "获取某用户发布的【未被接单】的跑腿需求")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "校验token", name = "Authorization", paramType = "header", required = true)
    })
    public R getUserUnOrder(int userId){
        List<ErrandFirstPageShowVO> errandFirstPageShowVOList = errandService.getUserUnOrder(userId);
        return R.ok().put("data", errandFirstPageShowVOList);
    }

    @PostMapping("/cancel")
    @ApiOperation(value = "取消跑腿")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "校验token", name = "Authorization", paramType = "header", required = true)
    })
    public R cancel(Integer errandId){
        errandService.cancel(errandId);
        return R.ok();
    }

}
