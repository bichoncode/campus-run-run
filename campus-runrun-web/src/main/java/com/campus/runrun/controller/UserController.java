package com.campus.runrun.controller;

import com.campus.runrun.common.utils.R;
import com.campus.runrun.model.vo.UserShowVO;
import com.campus.runrun.model.vo.UserUpdateVO;
import com.campus.runrun.service.api.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 *
 *
 * @author BichonCode
 * @email chenzhichaohh@163.com
 * @date 2020-10-31 17:12:39
 */
@Api(tags = "用户信息接口")
@RestController
@RequestMapping("api/user")
public class UserController {
    @Autowired
    private UserService userService;


    /**
     * 展示
     */
    @GetMapping("/show")
    public R show(int id){
        UserShowVO userShowVO = userService.showUser(id);
        return R.ok().put("data", userShowVO);
    }

    /**
     * 更新
     */
    @PostMapping("/update")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "校验token", name = "Authorization", paramType = "header", required = true)
    })
    public R update(UserUpdateVO userUpdateVO){
        userService.updateUser(userUpdateVO);
        return R.ok();
    }

}
