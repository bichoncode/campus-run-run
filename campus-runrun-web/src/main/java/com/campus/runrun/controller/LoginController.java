package com.campus.runrun.controller;


import com.campus.runrun.common.utils.R;
import com.campus.runrun.model.vo.UserLoginReqVO;
import com.campus.runrun.model.vo.UserVO;
import com.campus.runrun.service.api.LoginService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 登陆模块
 *
 * @author BichonCode
 * @email chenzhichaohh@163.com
 * @date 2020-07-31 00:55:44
 */
@Api(tags = "登陆模块")
@RestController
@RequestMapping("login")
public class LoginController {
    @Autowired
    private LoginService loginService;

    @ApiOperation(value = "微信用户登录", notes = "执行成功后返回用户对应的token")
    @PostMapping("/wxMiniProGramLogin")
    public R wxMiniProGramLogin(@RequestBody UserLoginReqVO userLoginReqVO) {
        UserVO userVO = loginService.wxMiniProGramLogin(userLoginReqVO);
        return R.ok().put("data", userVO);
    }
}
