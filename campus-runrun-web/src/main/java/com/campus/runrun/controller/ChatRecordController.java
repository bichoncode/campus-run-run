package com.campus.runrun.controller;


import com.campus.runrun.common.utils.PageResult;
import com.campus.runrun.common.utils.R;
import com.campus.runrun.model.entity.ChatRecordEntity;
import com.campus.runrun.model.requestvo.ChatRecordQueryParam;
import com.campus.runrun.model.vo.ChatRecordToClientVO;
import com.campus.runrun.service.api.ChatRecordService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.util.Arrays;

/**
 * 
 *
 * @author BichonCode
 * @email chenzhichaohh@163.com
 * @date 2020-10-31 17:12:39
 */
@RequestMapping("/api/chatRecord")
@RestController
@Api(tags = "私聊模块")
public class ChatRecordController {
    @Autowired
    private ChatRecordService chatRecordService;

    @PostMapping("/obtainRecordsPageByOppositeUserId")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "校验token", name = "Authorization", paramType = "header", required = true)
    })
    @ApiOperation(value = "点对点查询用户聊天记录")
    public R obtainRecordsPageByOppositeUserId(@RequestBody ChatRecordQueryParam chatRecordQueryParam) {

        PageResult pageResult = chatRecordService.obtainRecordsPageByOppositeUserId(chatRecordQueryParam);
        return R.ok().put("data", pageResult);
    }



    @PostMapping("/setHasReadByRecordId")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "校验token", name = "Authorization", paramType = "header", required = true)
    })
    @ApiOperation(value = "设置单条消息已读，单用户在聊天小窗口里面的时候，一接收到消息就调用此接口, 传消息id")
    public R setHasReadByRecordId( Integer id) {
        chatRecordService.setHasReadByRecordId(id);
        return R.ok();
    }

}
