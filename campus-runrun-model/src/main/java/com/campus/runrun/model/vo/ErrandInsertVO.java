package com.campus.runrun.model.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author BichonCode
 * @email chenzhichaohh@163.com
 * @date 2020-10-31 17:12:39
 */
@Data
public class ErrandInsertVO implements Serializable {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "内容描述",example = "帮我送点东西")
	private String content;

	@ApiModelProperty(value = "起点",example = "西三")
	private String startingPoint;

	/**
	 * 终点
	 */
	@ApiModelProperty(value = "终点",example = "东十三")
	private String destination;

	@ApiModelProperty(value = "预期费用",example = "9.99")
	private Double exceptedCost;


	@ApiModelProperty(value = "期待开始时间",example = "2020-12-25 09:00:00")
	private String expectStartTimeString;

	/**
	 * 期待结束时间
	 */
	@ApiModelProperty(value = "期待开始时间",example = "2020-12-26 09:00:00")
	private String expectEndTimeString;


}
