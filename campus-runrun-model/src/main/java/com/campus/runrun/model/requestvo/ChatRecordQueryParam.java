package com.campus.runrun.model.requestvo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author BichonCode
 * @mail chenzhichaohh@163.com
 * @create 2020-05-01
 */
@Data
@ToString
@ApiModel(value = "点对单的聊天记录查询参数")
public class ChatRecordQueryParam implements Serializable {

    private Integer page;

    private Integer size;

    @ApiModelProperty(value = "用户自身id", example = "1")
    private Integer fromUserId;

    @ApiModelProperty(value = "对方id", example = "2")
    private Integer toUserId;
}
