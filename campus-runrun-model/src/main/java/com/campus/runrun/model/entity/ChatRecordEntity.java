package com.campus.runrun.model.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author BichonCode
 * @email chenzhichaohh@163.com
 * @date 2020-10-31 17:12:39
 */
@Data
@TableName("chat_record")
public class ChatRecordEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId
	private Integer id;
	/**
	 * 发送者id
	 */
	private Integer fromUserId;
	/**
	 * 接收者id
	 */
	private Integer toUserId;
	/**
	 * 消息是否已读
	 */
	private Boolean hasRead;
	/**
	 * 消息内容
	 */
	private String messageContent;
	/**
	 * 消息类型
	 */
	private Integer messageType;
	/**
	 * 发送者是否删除
	 */
	private Boolean fromUserIsDelete;
	/**
	 * 接收者是否删除
	 */
	private Boolean toUserIsDelete;
	/**
	 * 
	 */
	private Date createTime;
	/**
	 * 
	 */
	private Date updateTime;

}
