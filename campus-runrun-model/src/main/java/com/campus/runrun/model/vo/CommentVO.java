package com.campus.runrun.model.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author BichonCode
 * @email chenzhichaohh@163.com
 * @date 2020-10-31 17:12:39
 */
@Data
@TableName("comment")
public class CommentVO implements Serializable {
	private static final long serialVersionUID = 1L;


	/**
	 * 评论id
	 */
	private Integer id;

	/**
	 * 评论内容
	 */
	private String content;

	/**
	 * 评论者头像
	 */
	private String headPortrait;

	/**
	 * 评论者昵称
	 */
	private String commentUsername;

	/**
	 * create_time
	 */
	private Date createTime;

}
