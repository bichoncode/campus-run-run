package com.campus.runrun.model.vo;

import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
public class ChatRecordToClientVO {
    private Integer id;


    private Integer fromUserId;

    private Integer toUserId;


    private Date createTime;

    private String messageContent;

    private Integer messageType;

}