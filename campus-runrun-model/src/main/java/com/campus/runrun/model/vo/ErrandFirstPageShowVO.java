package com.campus.runrun.model.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author chenyan
 * @createTime 2020/11/10 10:14
 * @description description
 */
@Data
public class ErrandFirstPageShowVO {


    @ApiModelProperty(value = "1")
    private Integer id;

    @ApiModelProperty(value = "发布者id", example = "1")
    private Integer employerId;

    @ApiModelProperty(value = "跑腿状态, 0表示未被接单，10表示已被接单， 20表示取消跑腿需求", example = "0")
    private Integer status;

    @ApiModelProperty(value = "发布文案（跑腿需求）", example = "拿外卖")
    private String content;

    @ApiModelProperty(value = "起点", example = "西三")
    private String startingPoint;

    @ApiModelProperty(value = "终点", example = "东十三")
    private String destination;

    @ApiModelProperty(value = "雇佣者用户名", example = "美少女")
    private String employerName;

    @ApiModelProperty(value = "预期费用", example = "2")
    private Double exceptedCost;

    @ApiModelProperty(value = "期待开始时间", example = "2")
    private Date expectStartTime;

    @ApiModelProperty(value = "期待到达时间", example = "2")
    private Date expectEndTime;

    @ApiModelProperty(value = "订单创建时间", example = "2")
    private Date createTime;

    @ApiModelProperty(value = "头像", example = "2")
    private String headPortrait;


}
