package com.campus.runrun.model.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@Data
@ToString
public class ChatListElementVO implements Serializable {
    @ApiModelProperty(value = "列表元素id",example = "2")
    private Integer id;

    @ApiModelProperty(value = "对方id",example = "2")
    private Integer toUserId;

    @ApiModelProperty(value = "对方昵称",example = "2")
    private String username;

    @ApiModelProperty(value = "对方头像",example = "2")
    private String headPortrait;

    @ApiModelProperty(value = "最后一条消息",example = "2")
    private String lastRecord;

    @ApiModelProperty(value = "是否有未读消息",example = "2")
    private Boolean whetherRead;

    @ApiModelProperty(value = "更新时间",example = "2")
    private Date updateTime;

}