package com.campus.runrun.model.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 
 * 
 * @author BichonCode
 * @email chenzhichaohh@163.com
 * @date 2020-10-31 17:12:39
 */
@Data
@TableName("user")
public class UserEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 
	 */
	private String openId;
	/**
	 * 昵称
	 */
	private String username;
	/**
	 * 校区(大学城、龙洞、东风路、番禺)
	 */
	private String campus;

	/**
	 * 头像路径
	 */
	private String headPortrait;


	/**
	 * 1为男，2为女
	 */
	private Integer sex;
	/**
	 * 成交数量
	 */
	private Integer dealCount;


	/**
	 * 接单者取消的数量
	 */
	private Integer cancelByEmploueeCount;


	/**
	 * 发布者取消数量
	 */
	private Integer cancelByEmplouerCount;

	/**
	 * 
	 */
	private Date createTime;
	/**
	 * 
	 */
	private Date updateTime;

}
