package com.campus.runrun.model.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author BichonCode
 * @email chenzhichaohh@163.com
 * @date 2020-10-31 17:12:39
 */
@Data
@TableName("comment")
public class CommentEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 评价者id
	 */
	private Integer userId;
	/**
	 * 订单id
	 */
	private Integer orderId;
	/**
	 * 父评论id，直接对订单评论的此id为-1，对订单评论的评论的，对订单的评论进行评论的，则pid与replyid相同，均为订单评论的id
	 */
	private Integer pId;
	/**
	 * 楼中楼之中你回复的评论的id，pid为订单评论的id
	 */
	private Integer replyId;
	/**
	 * 评论内容
	 */
	private String content;
	/**
	 * 
	 */
	private Date createTime;
	/**
	 * 
	 */
	private Date updateTime;

}
