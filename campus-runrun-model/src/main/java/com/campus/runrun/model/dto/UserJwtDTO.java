package com.campus.runrun.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Set;

/**
 * 用户表
 * 
 * @author BichonCode
 * @email chenzhichaohh@163.com
 * @date 2020-07-31 00:55:44
 */
@Data
public class UserJwtDTO implements Serializable {
	private static final long serialVersionUID = 1L;


	@ApiModelProperty(value = "用户id",example = "1")
	private Integer id;

	@ApiModelProperty(value = "用户名",example = "...........")
	private String username;

	@ApiModelProperty(value = "头像url",example = "...........")
	private String headImageUrl;

	@ApiModelProperty(value = "鉴权token",example = "...........")
	private String token;

	private String openId;

}
