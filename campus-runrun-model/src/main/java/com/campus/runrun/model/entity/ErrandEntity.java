package com.campus.runrun.model.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author BichonCode
 * @email chenzhichaohh@163.com
 * @date 2020-10-31 17:12:39
 */
@Data
@TableName("errand")
public class ErrandEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;

	/**
	 * 发布文案（跑腿需求）
	 */
	private String content;
	/**
	 * 起点
	 */
	private String startingPoint;
	/**
	 * 跑腿状态0代表未被接单，10代表已被接单,20代表取消跑腿需求
	 */
	private Integer status;
	/**
	 * 终点
	 */
	private String destination;
	/**
	 * 雇佣者id
	 */
	private Integer employerId;
	/**
	 * 预期费用
	 */
	private Double exceptedCost;
	/**
	 * 期待开始时间
	 */
	private Date expectStartTime;

	/**
	 * 期待结束时间
	 */
	private Date expectEndTime;

	/**
	 * 
	 */
	private Date createTime;
	/**
	 * 
	 */
	private Date updateTime;

}
