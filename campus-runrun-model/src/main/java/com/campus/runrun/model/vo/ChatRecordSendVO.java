package com.campus.runrun.model.vo;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class ChatRecordSendVO {

    private Integer fromUserId;

    private Integer toUserId;

    private String messageContent;

    private Integer messageType;

}