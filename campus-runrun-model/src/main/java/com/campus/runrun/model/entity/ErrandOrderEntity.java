package com.campus.runrun.model.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author BichonCode
 * @email chenzhichaohh@163.com
 * @date 2020-10-31 17:12:39
 */
@Data
@TableName("errand_order")
public class ErrandOrderEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 
	 */
	private Integer errandId;
	/**
	 * 订单状态，0代表交易中，10代表已成功，20代表交易取消
	 */
	private Integer status;
	/**
	 * 雇佣者id
	 */
	private Integer employerId;
	/**
	 * 跑腿id
	 */
	private Integer employeeId;
	/**
	 * 
	 */
	private Date createTime;
	/**
	 * 
	 */
	private Date updateTime;

}
