package com.campus.runrun.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.util.Date;

@Data
@ApiModel
public class OrderShowVO {

    @ApiModelProperty(value = "订单id",example = "1")
    private Integer id;

    @ApiModelProperty(value = "头像路径",example = "xxx")
    private String headPortrait;

    @ApiModelProperty(value = "用户名",example = "Rick")
    private String username;

    @ApiModelProperty(value = "订单状态：0-待接单，10-待对方确认，20-交易中，30-交易完成，40-交易取消",example = "0")
    private Integer status;

    @ApiModelProperty(value = "订单创建时间")
    private Date createTime;

    @ApiModelProperty(value = "发布文案",example = "找跑腿找跑腿")
    private String content;

    @ApiModelProperty(value = "起点",example = "二饭保安亭")
    private String startingPoint;

    @ApiModelProperty(value = "终点",example = "东13")
    private String destination;

    @ApiModelProperty(value = "预期费用",example = "555")
    private Double exceptedCost;

    @ApiModelProperty(value = "期待开始时间")
    private Date expectStartTime;

    @ApiModelProperty(value = "期待结束时间")
    private Date expectEndTime;

    @ApiModelProperty(value = "订单对方用户Id",example = "2")
    private Integer oppositeId;

    @ApiModelProperty(value = "是否是雇佣方",example = "true")
    private Boolean isEmployer;
}
