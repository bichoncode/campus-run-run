package com.campus.runrun.model.vo;/**
 * @author chenyan
 * @createTime 2020/11/20 11:30
 * @description description
 */

import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 *@description: TODO
 *@author: chenyan
 *@create: 2020/11/20 11:30
 */
@Data
public class UserUpdateVO {

    @TableId
    @ApiModelProperty(value = "id",example = "1")
    private Integer id;

    @ApiModelProperty(value = "昵称",example = "1")
    private String username;

    @ApiModelProperty(value = "校区（大学城、龙洞、东风路、番禺）",example = "大学城")
    private String campus;

    @ApiModelProperty(value = "头像url",example = "...........")
    private String headImageUrl;

}
