package com.campus.runrun.model.vo;/**
 * @author chenyan
 * @createTime 2020/11/20 10:09
 * @description description
 */

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 *@description: TODO
 *@author: chenyan
 *@create: 2020/11/20 10:09
 */
@Data
public class UserShowVO{
    @ApiModelProperty(value = "昵称",example = "1")
    private String username;

    @ApiModelProperty(value = "校区（大学城、龙洞、东风路、番禺）",example = "大学城")
    private String campus;

    @ApiModelProperty(value = "头像url",example = "...........")
    private String headPortrait;

    @ApiModelProperty(value = "性别",example = "1")
    private Integer sex;

    @ApiModelProperty(value = "成交数量",example = "23")
    private Integer dealCount;

    @ApiModelProperty(value = "跑腿取消订单数量",example = "23")
    private Integer cancelByEmploueeCount;

    @ApiModelProperty(value = "发布者取消数量",example = "23")
    private Integer cancelByEmplouerCount;
}
