package com.campus.runrun.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel
public class UserLoginReqVO implements Serializable {

    @ApiModelProperty(value = "wx.login返回的code",required = true,example = "84dsa5f48w6af45a6sf48wf4a56wf4saf4as8a")
    private String code;

    @ApiModelProperty(value = "用户名",required = true, example = "Czc超")
    private String username;

    @ApiModelProperty(value = "头像url",required = true, example = "https://smietaoblog.oss-cn-beijing.aliyuncs.com/blogphotos/20200610104610.jpg")
    private String headPortrait;
}